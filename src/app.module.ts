import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppointmentsModule } from './appointments/appointments.module';
import { BarbersModule } from './barbers/barbers.module';
import { ClientsModule } from './clients/clients.module';
import { FirebaseAuthStrategy } from './firebase/firebase-auth.strategy';
import { UsersModule } from './users/users.module';
import { MessagesModule } from './messages/messages.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async () => ({
        uri: process.env.MONGO_URL,
      }),
    }),
    MessagesModule,
    ConfigModule.forRoot(),
    UsersModule,
    ClientsModule,
    BarbersModule,
    AppointmentsModule
  ],
  controllers: [],
  providers: [FirebaseAuthStrategy],
})
export class AppModule { }
