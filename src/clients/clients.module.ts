import { Module } from "@nestjs/common";
import { ClientsRepository } from "./clients.repository";
import { ClientsController } from "./client.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { Client, ClientSchema } from "./schemas/client.model";
import { ClientsValidator } from "./clients.validator";
import { ClientsHelper } from "./clients.helper.";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Client.name, schema: ClientSchema }]),
  ],
  controllers: [ClientsController],
  providers: [ClientsRepository, ClientsValidator, ClientsHelper],
  exports: [
    ClientsRepository,
    ClientsValidator
  ],
})
export class ClientsModule { }
