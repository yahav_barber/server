import { IsString, IsPhoneNumber } from "class-validator";
import { ApiProperty, ApiResponseProperty } from "@nestjs/swagger";

export class Client {

  @ApiResponseProperty()
  _id: string;

  @IsString()
  @ApiProperty()
  name: string;

  @IsString()
  @IsPhoneNumber('IL')
  @ApiProperty()
  phoneNumber: string;
}
