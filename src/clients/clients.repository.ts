import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Client as ClientDto } from "./dtos/client.dto";
import { Client } from "./schemas/client.model";

@Injectable()
export class ClientsRepository {
  constructor(@InjectModel(Client.name) private readonly clientsModel: Model<Client>) {
  }

  async createClient(client: ClientDto) {
    const createdUser = new this.clientsModel(client);
    return createdUser.save();
  }

  async getById(clientId: string) {
    return this.clientsModel.findById(clientId).exec();
  }

  async getAllClients() {
    return this.clientsModel.find().exec();

  }

  async updateClient(client: Client) {
    return this.clientsModel.updateOne({ _id: client._id }, client).exec();
  }

  async getClientByPhoneNumber(phoneNumber: string) {
    return this.clientsModel.findOne({ phoneNumber }).exec();
  }
}
