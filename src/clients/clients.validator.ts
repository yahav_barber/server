import { Injectable, NotFoundException } from "@nestjs/common";
import { ClientsRepository } from "./clients.repository";

@Injectable()
export class ClientsValidator {
    constructor(private clientsRepository: ClientsRepository) {
    }

    async throwErrorIfClientIdDoesNotExist(clientId: string) {
        const existClient = await this.clientsRepository.getById(clientId);
        if (!existClient) {
            throw new NotFoundException(`id ${clientId} not found`);
        }

        return existClient;
    }

    async getOrThrowIfPhoneNumberDoesNotExist(phoneNumber: string) {
        const existUser = await this.clientsRepository.getClientByPhoneNumber(phoneNumber);
        if (!existUser) {
            throw new NotFoundException(`phone number doesn't exist`);
        }

        return existUser;
    }
}