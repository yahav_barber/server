import {
  Controller,
  Post,
  Body,
  HttpCode,
  HttpStatus,
  Query,
  Get,
} from "@nestjs/common";
import { ApiTags, ApiCreatedResponse, ApiQuery, ApiBearerAuth } from "@nestjs/swagger";
import { Client } from "./dtos/client.dto";
import { ClientsHelper } from "./clients.helper.";
import { ClientsRepository } from "./clients.repository";

@Controller("clients")
@ApiBearerAuth('access-token')
@ApiTags("Clients")
export class ClientsController {
  constructor(
    private clientsRepository: ClientsRepository,
    private clientsHelper: ClientsHelper
  ) { }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({
    status: HttpStatus.CREATED,
    description: "Create new client",
    type: Client,
  })
  async addClient(@Body() createdClient: Client) {
    return await this.clientsRepository.createClient(createdClient);
  }

  @Get()
  @ApiQuery({
    name: 'phoneNumber',
    type: String,
    required: false
  })
  async getUserByParams(@Query('phoneNumber') phoneNumber) {
    const user = await this.clientsRepository.getClientByPhoneNumber(phoneNumber);

    this.clientsHelper.handleGetUserResponse(user);

    return user;
  }
}
