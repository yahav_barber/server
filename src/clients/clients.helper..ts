import { Injectable, NotFoundException } from "@nestjs/common";
import * as _ from 'lodash'
import { Client } from "./schemas/client.model";

@Injectable()
export class ClientsHelper {
    handleGetUserResponse(client: Client) {
        if (!client) {
            throw new NotFoundException(`Not found user`);
        }
    }
}