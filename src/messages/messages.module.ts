import { Global, Module } from "@nestjs/common";
import { MessagesSender } from "./messages.sender";
import { TwilioModule } from "nestjs-twilio";
import * as messsagesConfig from './messages.config.json'

@Global()
@Module({
  imports: [
    TwilioModule.forRoot({
      accountSid: messsagesConfig.accountSid,
      authToken: messsagesConfig.authToken,
    }),
  ],
  controllers: [],
  providers: [MessagesSender],
  exports: [MessagesSender],
})
export class MessagesModule { }
