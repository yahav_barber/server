import { Injectable } from "@nestjs/common";
import * as _ from 'lodash'
import { InjectTwilio, TwilioClient } from "nestjs-twilio";
import { DateRange, formatDateToTime, getFormattedDate } from "src/common/dates.helper";
import * as messsagesConfig from './messages.config.json'


@Injectable()
export class MessagesSender {
    constructor(@InjectTwilio() private readonly client: TwilioClient) {
    }

    async scheduleAppointmentMessage(clientPhone: string, dateRange: DateRange) {
        const appointmentTime = formatDateToTime(dateRange.fromDate)
        const splittedDate = getFormattedDate(dateRange.fromDate, true).split("-");
        const appointmentDate = `${splittedDate[0]}/${splittedDate[1]}`;

        this.sendMessage(`הזמנת תור בשעה ${appointmentTime} בתאריך ${appointmentDate}`, clientPhone);
    }

    private sendMessage(body: string, to: string, timeout = 2000) {
        setTimeout(() => {
            this.client.messages.create({
                body,
                from: messsagesConfig.senderName,
                to
            });
        }, timeout)
    }
}