import { forwardRef, Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AppointmentsModule } from "src/appointments/appointments.module";
import { ClientsModule } from "../clients/clients.module";
import { BarbersController } from "./barbers.controller";
import { BarbersHelper } from "./barbers.helper";
import { BarbersRepository } from "./barbers.repository";
import { BarbersValidator } from "./barbers.validator";
import { Barber, BarberSchema } from "./schemas/barber.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Barber.name, schema: BarberSchema }]),
    ClientsModule,
    forwardRef(() => AppointmentsModule),
  ],
  controllers: [BarbersController],
  providers: [BarbersValidator, BarbersHelper, BarbersRepository],
  exports: [BarbersValidator, BarbersHelper, BarbersRepository],
})
export class BarbersModule { }
