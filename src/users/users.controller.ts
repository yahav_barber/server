import {
  Controller,
  Query,
  Get
} from "@nestjs/common";
import { ApiTags, ApiQuery, ApiBearerAuth } from "@nestjs/swagger";
import { UsersHelper } from "./users.helper";

@Controller("users")
@ApiBearerAuth('access-token')
@ApiTags("Users")
export class UserController {
  constructor(
    private usersHelper: UsersHelper,
  ) { }

  @Get()
  @ApiQuery({
    name: 'phoneNumber',
    type: String,
    required: false
  })
  async getUserByParams(@Query('phoneNumber') phoneNumber) {
    return await this.usersHelper.getUserByPhoneNumber(phoneNumber);
  }
}
