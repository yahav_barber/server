import { Module } from "@nestjs/common";
import { BarbersModule } from "src/barbers/barbers.module";
import { ClientsModule } from "src/clients/clients.module";
import { UserController } from "./users.controller";
import { UsersHelper } from "./users.helper";

@Module({
  imports: [ClientsModule, BarbersModule],
  controllers: [UserController],
  providers: [UsersHelper],
  exports: [UsersHelper],
})
export class UsersModule { }
