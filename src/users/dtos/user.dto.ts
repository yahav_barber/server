import { IsEnum } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { UserType } from "../enums/userType";
import { Client } from "src/clients/dtos/client.dto";
import { Barber } from "src/barbers/dtos/barber.dto";

export class User {
  @ApiProperty()
  @IsEnum(UserType)
  userType: UserType;

  @ApiProperty()
  user: Client | Barber;
}
