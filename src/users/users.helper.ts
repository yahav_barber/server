import { Injectable, NotFoundException } from "@nestjs/common";
import * as _ from 'lodash'
import { BarbersRepository } from "../barbers/barbers.repository";
import { ClientsRepository } from "../clients/clients.repository";
import { User } from "./dtos/user.dto";
import { UserType } from "./enums/userType";

@Injectable()
export class UsersHelper {
    constructor(private clientsRepository: ClientsRepository,
        private barbersRepository: BarbersRepository) { }

    async getUserByPhoneNumber(phoneNumber: string): Promise<User | {}> {
        const fitBarber = await this.barbersRepository.getByPhoneNumber(phoneNumber);

        if (!_.isEmpty(fitBarber)) {
            return {
                userType: UserType.BARBER,
                user: fitBarber
            }
        }

        const fitClient = await this.clientsRepository.getClientByPhoneNumber(phoneNumber);

        if (!_.isEmpty(fitClient)) {
            return {
                userType: UserType.CLIENT,
                user: fitClient
            }
        }

        return {};
    }
}